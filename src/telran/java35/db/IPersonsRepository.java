package telran.java35.db;

import java.util.List;

import telran.java35.db.dao.Gender;
import telran.java35.db.dao.PersonDao;

public interface IPersonsRepository {
	List<PersonDao> getAllPersons();
	List<PersonDao> findAllByGender(Gender gender);
	List<PersonDao> findAllByGenderWhereSalaryGreatOrEquals(Gender gender, int salary);
}
