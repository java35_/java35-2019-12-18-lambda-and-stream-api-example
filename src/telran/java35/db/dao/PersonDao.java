package telran.java35.db.dao;

import java.time.LocalDateTime;

public class PersonDao {
	long id;
	String name;
	int salary;
	Gender gender;
	String department;
	LocalDateTime lastUpdate;
	
	
	public PersonDao(long id, String name, int salary, Gender gender, String department) {
		this.id = id;
		this.name = name;
		this.salary = salary;
		this.gender = gender;
		this.department = department;
		this.lastUpdate = LocalDateTime.now();
	}
	public long getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public int getSalary() {
		return salary;
	}
	public Gender getGender() {
		return gender;
	}
	public String getDepartment() {
		return department;
	}
	public LocalDateTime getLastUpdate() {
		return lastUpdate;
	}
	
	
}
