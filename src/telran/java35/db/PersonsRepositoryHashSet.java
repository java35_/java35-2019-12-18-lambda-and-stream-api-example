package telran.java35.db;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import telran.java35.db.dao.Gender;
import telran.java35.db.dao.PersonDao;

public class PersonsRepositoryHashSet implements IPersonsRepository{

	Set<PersonDao> persons = new HashSet<>();
	
	@Override
	public List<PersonDao> getAllPersons() {
		return persons.stream()
				.collect(Collectors.toList());
	}

	@Override
	public List<PersonDao> findAllByGender(Gender gender) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<PersonDao> findAllByGenderWhereSalaryGreatOrEquals(Gender gender, int salary) {
		// TODO Auto-generated method stub
		return null;
	}

}
