package telran.java35.db;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import telran.java35.db.dao.Gender;
import telran.java35.db.dao.PersonDao;

public class PersonsRepository implements IPersonsRepository {
	private List<PersonDao> persons;
	
	public PersonsRepository() {
		persons = new ArrayList<>();
	}
	
	public PersonsRepository(List<PersonDao> persons) {
		this.persons = persons;
	}

	public List<PersonDao> getAllPersons() {
		return new ArrayList<PersonDao>(persons);
	}

	public List<PersonDao> findAllByGender(Gender gender) {
		return persons.stream()
					.filter(x -> x.getGender() == gender)
					.collect(Collectors.toList());
	}

	public List<PersonDao> findAllByGenderWhereSalaryGreatOrEquals(Gender gender, int salary) {
		return persons.stream()
				.filter(x -> x.getGender() == gender && x.getSalary() >= salary)
				.collect(Collectors.toList());
	}
}
