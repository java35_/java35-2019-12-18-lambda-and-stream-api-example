package telran.java35.ui;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

import telran.java35.db.dao.Gender;
import telran.java35.db.dao.PersonDao;
import telran.java35.service.PersonsService;
import telran.java35.ui.dto.PersonDto;

public class PersonsController {
	
	PersonsService service;
	
	public PersonsController(PersonsService service) {
		this.service = service;
	}
	
	public void start() {
		Scanner sc = new Scanner(System.in);
		int answer = 0;
		do {
			System.out.println("Please enter value from 1 to 6");
			answer = sc.nextInt();
			
			switch (answer) {
				case 1:
//					List<PersonDto> persons = service.getAllPersons();
					service.getAllPersons()
							.forEach(System.out::println);
					break;
				case 2:
					service.findAllByGender(Gender.FEMALE)
							.forEach(System.out::println);
					break;
				case 3:
					service.findAllByGenderWhereSalaryGreatOrEquals(Gender.MALE, 13_000)
							.forEach(System.out::println);
					break;
				case 4:
					Map<String, List<PersonDto>> map;
					map = service.personsByDepartments();
					map.entrySet().stream()
						.forEach(x -> {
							System.out.println(x.getKey());
							x.getValue().forEach(v -> System.out.println("    " + v));
						});
					break;
				default:
					break;
				}
			
		} while (answer > 0 && answer < 6);
	}
}
