package telran.java35.ui.dto;

import telran.java35.db.dao.Gender;

public class PersonDto {
	long id;
	String name;
	int salary;
	Gender gender;
	String department;
	
	public PersonDto(long id, String name, int salary, Gender gender, String department) {
		this.id = id;
		this.name = name;
		this.salary = salary;
		this.gender = gender;
		this.department = department;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getSalary() {
		return salary;
	}

	public Gender getGender() {
		return gender;
	}

	public String getDepartment() {
		return department;
	}

	@Override
	public String toString() {
		return "PersonDto [id=" + id + ", name=" + name + ", salary=" + salary + ", gender=" + gender + ", department="
				+ department + "]";
	}
	
	
}
