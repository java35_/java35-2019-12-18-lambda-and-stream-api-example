package telran.java35;

import java.lang.ModuleLayer.Controller;
import java.util.ArrayList;
import java.util.List;

import telran.java35.db.IPersonsRepository;
import telran.java35.db.PersonsRepository;
import telran.java35.db.PersonsRepositoryHashSet;
import telran.java35.db.dao.Gender;
import telran.java35.db.dao.PersonDao;
import telran.java35.service.PersonsService;
import telran.java35.ui.PersonsController;

public class PersonsAppl {

	public static void main(String[] args) {
		
		PersonDao person1 = new PersonDao(1, "name1", 10_000, Gender.FEMALE, "Department1");
		PersonDao person2 = new PersonDao(2, "name2", 15_000, Gender.MALE, "Department1");
		PersonDao person3 = new PersonDao(3, "name3", 13_000, Gender.FEMALE, "Department1");
		PersonDao person4 = new PersonDao(4, "name4", 8_000, Gender.MALE, "Department1");
		PersonDao person5 = new PersonDao(5, "name5", 6_000, Gender.MALE, "Department1");
		
		PersonDao person6 = new PersonDao(6, "name6", 5_000, Gender.FEMALE, "Department2");
		PersonDao person7 = new PersonDao(7, "name7", 6_000, Gender.FEMALE, "Department2");
		PersonDao person8 = new PersonDao(8, "name8", 7_000, Gender.FEMALE, "Department2");
		PersonDao person9 = new PersonDao(9, "name9", 10_000, Gender.MALE, "Department2");
		PersonDao person10 = new PersonDao(10, "name10", 13_000, Gender.MALE, "Department2");

		PersonDao person11 = new PersonDao(11, "name11", 11_000, Gender.MALE, "Department3");
		
		List<PersonDao> newPersons = new ArrayList<>();
		
		newPersons.add(person1);
		newPersons.add(person2);
		newPersons.add(person3);
		newPersons.add(person4);
		newPersons.add(person5);
		newPersons.add(person6);
		newPersons.add(person7);
		newPersons.add(person8);
		newPersons.add(person9);
		newPersons.add(person10);
		newPersons.add(person11);
		
		IPersonsRepository repository = new PersonsRepositoryHashSet();
		PersonsService service = new PersonsService(repository);
		PersonsController controller = new PersonsController(service);
		
		controller.start();
	}

}
