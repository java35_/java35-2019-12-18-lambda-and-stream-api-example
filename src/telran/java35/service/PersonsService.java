package telran.java35.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import telran.java35.db.IPersonsRepository;
import telran.java35.db.PersonsRepository;
import telran.java35.db.PersonsRepositoryHashSet;
import telran.java35.db.dao.Gender;
import telran.java35.db.dao.PersonDao;
import telran.java35.ui.dto.PersonDto;

public class PersonsService {
	
	IPersonsRepository repo;
	
	public PersonsService(IPersonsRepository repo) {
		this.repo = repo;
	}
	
	public List<PersonDto> getAllPersons() {
		return repo.getAllPersons().stream()
				.map(this::cnvPersonDaoToDto)
				.collect(Collectors.toList());
//		List<PersonDto> persons = new ArrayList<>();
//		int i = 0;
//		repo.getAllPersons()
//			.forEach(x -> persons.add(cnvPersonDaoToDto(x)));
//			.forEach(x -> i = (int)x.getId());
	}

	private PersonDto cnvPersonDaoToDto(PersonDao personDao) {
		return new PersonDto(personDao.getId(), 
							personDao.getName(), 
							personDao.getSalary(), 
							personDao.getGender(),
							personDao.getDepartment());
	}

	public List<PersonDto> findAllByGender(Gender gender) {
		return repo.findAllByGender(gender).stream()
					.map(this::cnvPersonDaoToDto)
					.collect(Collectors.toList());
	}

	public List<PersonDto> findAllByGenderWhereSalaryGreatOrEquals(Gender gender, int salary) {
		return repo.findAllByGenderWhereSalaryGreatOrEquals(gender, salary)
				.stream()
				.map(this::cnvPersonDaoToDto)
				.collect(Collectors.toList());
	}

	public Map<String, List<PersonDto>> personsByDepartments() {
		return repo.getAllPersons().stream()
				.map(this::cnvPersonDaoToDto)
				.collect(Collectors.groupingBy(x -> (String)x.getDepartment()));
	}
}
